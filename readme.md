Divers scripts utiles pour la distribution éducative PrimTux.

**cifs-libreoffice.sh**

Le script corrige le problèe suivant :  quand on sauvegarde un fichier existant sur un montage Samba (CIFS) Libreoffice indique "Impossible de créer le fichier de sauvegarde". C'est lié à la fonctionnalité de sauvegarde automatique. Ce script la désactive.

**proxy-eole.sh**

Permet d'automatiser le paramétrage du proxy de PrimTux pour une utilisation au sein d'un réseau EOLE. Utile lors d'un déploiement.

**Scripts Tetras-libre**

Pour le déploiement de PrimTux sur les écoles Grenoble avec un serveur EOLE, l'entreprise Tetras-libre a développé divers scripts en licence libre :

- [Ce commit](https://gitlab.tetras-libre.fr/tetras-libre/primtux-eole/commit/b5d6e35162f12ebf53fef277ab9bb001c08cdd44) corrigant un problème de synchronisation ntp derrière le proxy Eole voir

- Scripts et fichiers pour la configuration auto des videos projecteurs : [rule udev](https://gitlab.tetras-libre.fr/tetras-libre/primtux-eole/blob/master/40-display.rules), [script d'installation](https://gitlab.tetras-libre.fr/tetras-libre/primtux-eole/blob/master/set_screens.sh), [script de configuration](https://gitlab.tetras-libre.fr/tetras-libre/primtux-eole/blob/master/refreshVP.sh), attention il y a des path absolus

- [Script ](https://gitlab.tetras-libre.fr/tetras-libre/primtux-eole/blob/master/mount_shares.sh)et [service systemd](https://gitlab.tetras-libre.fr/tetras-libre/primtux-eole/blob/master/mount_shares.service) pour monter des partages samba une fois qu'il y a du réseau et pas avant. Hypothèses : les montages sont définis dans le fstab, le nom du workgroup est ecole-dc

**apt-cacher-ng**
https://github.com/CyrilleBiot/scripts/blob/master/acn.py

Script python d'installation et de configuration du serveur de cache apt :
- soit en tant que serveur (ajout du paquet sur le système), 
- soit en tant que client (cration d'un fichier de proxy apt).

Ce script permettra de mettre à jour les PC d'un réseau en utilisant l'un deux comme serveur de paquets.