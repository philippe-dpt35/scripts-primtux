#!/bin/bash

# Copyright (C) 2018  Tetras Libre <Contact@Tetras-Libre.fr>
# Author: Beniamine, David <David.Beniamine@Tetras-Libre.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

if [ "`whoami`" != "root" ]
then
    echo "This script should be run as root"
    exit 1
fi

cd /home
for u in *
do
        cd $u/.config/libreoffice/4/user
        sed -i.bak -e 's@^\(.*CreateBackup.*\)true\(.*\)$@\1false\2@' registrymodifications.xcu
        diff registrymodifications.xcu registrymodifications.xcu.bak
    cd /home
done
