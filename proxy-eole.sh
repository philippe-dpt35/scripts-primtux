#!/bin/bash

# Auteur : philippe Ronflette - philippe.dpt35@primtux.fr

# Ajoute les variables d'environnement
echo "export http_proxy=192.168.0.3:3128
export https_proxy=192.168.0.3:3128
export ftp_proxy=192.168.0.3:3128" >>/etc/environment

echo 'Defaults env_keep = "http_proxy https_proxy ftp_proxy"' > /etc/sudoers.d/fix-env-var

# Ajoute le proxy pour apt
fichier=/etc/apt/apt.conf.d/10proxy
echo 'Acquire::http::proxy "http://192.168.0.3:3128/";' >"$fichier"
echo 'Acquire::https::proxy "https://192.168.0.3:3128/";' >>"$fichier"
echo 'Acquire::ftp::proxy "ftp://192.168.0.3:3128/";'  >>"$fichier"
chmod 0440 "$fichier"
